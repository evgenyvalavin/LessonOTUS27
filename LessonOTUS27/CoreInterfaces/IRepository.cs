﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.CoreInterfaces
{
    interface IRepository
    {
        Task<T> GetById(int id);
        Task<IReadOnlyList<T>> GetAll();
        Task<T> Add();
        Task Update();
        Task Clear();
        Task Remove();
    }
}