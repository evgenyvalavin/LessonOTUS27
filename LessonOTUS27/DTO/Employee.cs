﻿namespace Core.DTO
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int DepartamentId { get; set; }
     }
}
