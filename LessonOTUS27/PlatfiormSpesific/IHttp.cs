﻿using Core.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.PlatfiormSpesific
{
    public interface IHttp
    {
        Task<(MessageCode, T)> HTTP<T>(HttpMethodType type, string url, string request = "");
        void ClearRequestHeaders();
        void TryAddHeader(string name, string value);
        void RemoveHeader(string name);
    }
}
