﻿using System;

namespace Core.Services
{
    public interface ITimeService
    {
        DateTime Now { get; }
        DateTime UtcNow { get; }
    }
}