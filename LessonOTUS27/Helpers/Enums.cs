﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Helpers
{
    public enum HttpMethodType
    {
        Get,
        Post
    }

    public enum MessageCode
    {
        Ok,
        UndefinedError,
        UndefinedHttpError,
        UndefinedNetworkError,
        Unauthorized,
        BadRequest,
        NotFound,
        InternalServerError,
        ServiceUnavailable,
        GatewayTimeout,
        BadGateway,
        CourierNotFound,
        DomainNameError,
        NetworkUnreachable,
        ConnectionWasNotEstablished,
        NoRouteToHost,
        HttpTimeout,
        NTPtimeError,
    }
}
